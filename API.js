import {SERVER_ADDR} from "./Config.js"

const call = async (url) => {
    const request = await fetch(SERVER_ADDR + url)
    const result = await request.json();
    return result
}



export const allProducts = async (sort) => await call(`products${sort ? `?sort=${sort} ` :''}`);
  




export const allCategories = async () => await call('products/categories')




