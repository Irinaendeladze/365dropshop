import { allProducts } from "./API.js";
import { contentProductHTML } from "./CatalogContent.js";

export const catalog = document.getElementsByClassName('catalog')[0];

const fillCatalog = async (sort = null) => {
    const products = await allProducts(sort);
    let productHTML = " ";
    for (const product of products) {
        productHTML += contentProductHTML(product);
    }
     catalog.innerHTML = productHTML;
};

fillCatalog()

const sort = document.getElementById("sort");
sort.addEventListener('change', () => {
    fillCatalog(sort.value);
});


const searchInput = document.getElementById("searchInput");
const searchButton = document.getElementById("searchButton");
searchButton.addEventListener("click", () => {
    allProducts().then(products => {
        const searchResult = products.filter(item => item.title.includes(searchInput.value));
        let productHTML = " ";
        for (const product of searchResult) {
            productHTML += contentProductHTML(product);
        }
        catalog.innerHTML = productHTML;
    });
});