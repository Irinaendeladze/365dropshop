
export const contentProductHTML = (product) => {
    const productHTML =
        `<div class="catalog__product">
<div class="round">
    <input type="checkbox" id="checkbox" />
    <label for="checkbox"></label>
</div>
 <div class = "catalog__photo">
    <img src="${product.image}"/>
    </div>
<div class ='catalog__title'>
      ${product.title}
 </div>
<div class="catalog__prices">
 ${product.price}$
</div>
</div>
`;
    return productHTML
}

